/*
 * appinputtest.cpp
 *
 * Definition of AppEventtest, the main application object.
 */

#include "appinputtest.h"
#include <cstool/initapp.h>
#include <csutil/csstring.h>
#include <csutil/event.h>
#include <csutil/sysfunc.h>
#include <csutil/syspath.h>
#include <iutil/cmdline.h>
#include <iutil/csinput.h>
#include <iutil/event.h>
#include <iutil/eventq.h>
#include <iutil/plugin.h>
#include <iutil/vfs.h>

AppEventtest::AppEventtest () : csApplicationFramework ()
{
	SetApplicationName ("inputtest");
	logging = false;
	detect_mode = false;
	silent_mode = false;
}

AppEventtest::~AppEventtest ()
{
}

void AppEventtest::ProcessFrame ()
{
	if (g3d->BeginDraw (engine->GetBeginDrawFlags () | CSDRAW_3DGRAPHICS))
	{
		// Draw frame.
	}
}

void AppEventtest::FinishFrame ()
{
	g3d->FinishDraw ();
	g3d->Print (0);
}

bool AppEventtest::OnJoystickMove (iEvent &event)
{
	return PrintEvent (event);
}

bool AppEventtest::OnJoystickDown (iEvent &event)
{
	return PrintEvent (event);
}

bool AppEventtest::OnJoystickUp (iEvent &event)
{
	return PrintEvent (event);
}

bool AppEventtest::OnKeyboard (iEvent &event)
{
	return PrintEvent (event);
}

bool AppEventtest::OnMouseMove (iEvent &event)
{
	return PrintEvent (event);
}

bool AppEventtest::OnMouseDown (iEvent &event)
{
	return PrintEvent (event);
}

bool AppEventtest::OnMouseUp (iEvent &event)
{
	return PrintEvent (event);
}

bool AppEventtest::OnMouseClick (iEvent &event)
{
	return PrintEvent (event);
}

bool AppEventtest::OnMouseDoubleClick (iEvent &event)
{
	return PrintEvent (event);
}

bool AppEventtest::OnUnhandledEvent (iEvent &event)
{
	if (event.Name == csevCommandLineHelp (object_reg))
	{
		csPrintf ("inputtest [options] [logfilename]\n");
		csPrintf ("Options for inputtest:\n");
		csPrintf ("  %-18s Report all events, not just input. (no)\n", "-[no]allevents");
		csPrintf ("  %-18s Run in detection mode, instead of dump mode. (no)\n", "-[no]detect");
		csPrintf ("  %-18s Don't print any output to the console. (no)\n", "-[no]silent");
		csPrintf ("  %-18s Log all test results. (no)\n", "-[no]log");
		return true;
	}
	else
	{
		return (all_events ? PrintEvent (event) : false);
	}
}

bool AppEventtest::PrintEvent (iEvent& ev)
{
	csPrintf ("Event received: %s\n", csEventNameRegistry::GetRegistry (GetObjectRegistry ())->GetString (ev.GetName ()));
	csRef<iEventAttributeIterator> attribit = ev.GetAttributeIterator ();
	csString attribname;
	while (attribit->HasNext ())
	{
		attribname = attribit->Next ();
		switch (ev.GetAttributeType (attribname))
		{
		case csEventAttrInt:
		{
			int attribvalue;
			ev.Retrieve (attribname, attribvalue);
			csPrintf ("	attribute: %s = %d\n", attribname.GetData (), attribvalue);
			break;
		}
		case csEventAttrUInt:
		{
			unsigned int attribvalue;
			ev.Retrieve (attribname, attribvalue);
			csPrintf ("	attribute: %s = %u\n", attribname.GetData (), attribvalue);
			break;
		}
		case csEventAttrFloat:
		{
			float attribvalue;
			ev.Retrieve (attribname, attribvalue);
			csPrintf ("	attribute: %s = %f\n", attribname.GetData (), attribvalue);
			break;
		}
		case csEventAttrDatabuffer:
		{
			char* attribvalue;
			ev.Retrieve (attribname, (const char *&) attribvalue);
			csString attribstring = "";
			for (size_t i = 0; i < strlen (attribvalue); i++)
			{
				attribstring.Format ("%s %02x", attribstring.GetData (), (int8) attribvalue[i]);
			}
			csPrintf ("	attribute: %s = %s (%u bytes)\n", attribname.GetData (), attribstring.LTrim ().GetData (), strlen (attribvalue));
			break;
		}
		case csEventAttrEvent:
			csPrintf ("	attribute: %s = Event\n", attribname.GetData ());
			break;
		case csEventAttriBase:
			csPrintf ("	attribute: %s = iBase\n", attribname.GetData ());
			break;
		default:
			csPrintf ("	attribute: %s of unknown type\n", attribname.GetData ());
		}
	}
	fflush (stdout);
	return false;
}

bool AppEventtest::OnInitialize (int argc, char* argv[])
{
	iObjectRegistry* object_reg = GetObjectRegistry ();

	// Load application-specific configuration file.
	if (!csInitializer::SetupConfigManager (object_reg, "/this/AppInputtest.cfg", GetApplicationName ()))
	{
		return ReportError ("Failed to initialize configuration manager!");
	}

	// RequestPlugins () will load all plugins we specify.  In addition it will
	// also check if there are plugins that need to be loaded from the
	// configuration system (both the application configuration and CS or global
	// configurations).  It also supports specifying plugins on the command line
	// via the --plugin= option.
	if (!csInitializer::RequestPlugins (object_reg,
		CS_REQUEST_VFS,
		CS_REQUEST_OPENGL3D,
		CS_REQUEST_ENGINE,
		CS_REQUEST_FONTSERVER,
		CS_REQUEST_IMAGELOADER,
		CS_REQUEST_LEVELLOADER,
		CS_REQUEST_REPORTER,
		CS_REQUEST_REPORTERLISTENER,
		CS_REQUEST_END))
	{
		return ReportError ("Failed to initialize plugins!");
	}

	// "Warm up" the event handler so it can interact with the world
	csBaseEventHandler::Initialize (GetObjectRegistry ());
 
	// Set up an event handler for the application.  Crystal Space is fully
	// event-driven.  Everything (except for this initialization) happens in
	// response to an event.
	if (!RegisterQueue (object_reg, csevAllEvents (GetObjectRegistry ())))
	{
		return ReportError ("Failed to set up event handler!");
	}

	return true;
}

void AppEventtest::OnExit ()
{
}

bool AppEventtest::Application ()
{
	iObjectRegistry* object_reg = GetObjectRegistry ();

	// Open the main system. This will open all the previously loaded plugins
	// (i.e. all windows will be opened).
	if (!OpenApplication (object_reg))
	{
		return ReportError ("Error opening system!");
	}

	// Attempt to load a joystick plugin.
	csRef<iStringArray> joystickClasses =
		iSCF::SCF->QueryClassList ("crystalspace.device.joystick.");
	if (joystickClasses.IsValid ())
	{
		csRef<iPluginManager> plugmgr = csQueryRegistry<iPluginManager> (object_reg);
		for (size_t i = 0; i < joystickClasses->Length (); i++)
		{
			const char* className = joystickClasses->Get (i);
			iBase* pluginClass = plugmgr->LoadPlugin (className);

			ReportInfo ("Attempt to load plugin '%s' %s", className,
				 (pluginClass != 0) ? "successful" : "failed");
			if (pluginClass != 0)
			{
				pluginClass->DecRef ();
			}
		}
	}

	// Now get the pointer to various modules we need.  We fetch them from the
	// object registry.  The RequestPlugins () call we did earlier registered all
	// loaded plugins with the object registry.  It is also possible to load
	// plugins manually on-demand.
	g3d = csQueryRegistry<iGraphics3D> (object_reg);
	if (!g3d)
	{
		return ReportError ("Failed to locate 3D renderer!");
	}

	engine = csQueryRegistry<iEngine> (object_reg);
	if (!engine)
	{
		return ReportError ("Failed to locate 3D engine!");
	}

	csRef<iCommandLineParser> cmdline = csQueryRegistry<iCommandLineParser> (object_reg);

	if (cmdline->GetBoolOption ("allevents"))
	{
		all_events = true;
	}

	if (cmdline->GetBoolOption ("detect"))
	{
		detect_mode = true;
	}

	if (cmdline->GetBoolOption ("silent"))
	{
		silent_mode = true;
	}

	if (!(logfilename = cmdline->GetName ()).IsEmpty ())
	{
		logging = true;
	}
	else if (cmdline->GetBoolOption ("log"))
	{
		logging = true;
		logfilename = "/this/input.log";
	}

	if (logging)
	{
		csRef<iStandardReporterListener> reporter = csQueryRegistry<iStandardReporterListener> (object_reg);
		reporter->SetDebugFile (logfilename);
		//reporter->SetMessageDestination (CS_REPORTER_SEVERITY_NOTIFY
	}

	// Start the default run/event loop.  This will return only when some code,
	// such as OnKeyboard (), has asked the run loop to terminate.
	Run ();

	return true;
}
